import { Navigation } from 'react-native-navigation'
import { Provider } from 'react-redux'

import StartScreen from './src/screens/StartScreen/StartScreen'
import CodeScreen from './src/screens/CodeScreen/CodeScreen'
import CreateProjectScreen from './src/screens/CreateProjectScreen/CreateProjectScreen'
import LayerConfigScreen from './src/screens/LayerConfigScreen/LayerConfigScreen'
import ProjectsScreen from './src/screens/ProjectsScreen/ProjectsScreen'
import EditProjectScreen from './src/screens/EditProjectScreen/EditProjectScreen'
import PointsPreviewScreen from './src/screens/PointsPreviewScreen/PointsPreviewScreen'
import Info from './src/screens/InfoScreen/InfoScreen'
import startApp from './src/screens/StartApp/StartApp'

import configurateStore from './src/store/configStore'

const store = configurateStore()

Navigation.registerComponent("geovcs.StartScreen", () => StartScreen, store, Provider)
Navigation.registerComponent("geovcs.CodeScreen", () => CodeScreen, store, Provider)
Navigation.registerComponent("geovcs.CreateProjectScreen", () => CreateProjectScreen, store, Provider)
Navigation.registerComponent("geovcs.LayerConfigScreen", () => LayerConfigScreen, store, Provider)
Navigation.registerComponent("geovcs.ProjectsScreen", () => ProjectsScreen, store, Provider)
Navigation.registerComponent("geovcs.EditProjectScreen", () => EditProjectScreen, store, Provider)
Navigation.registerComponent("geovcs.PointsPreviewScreen", () => PointsPreviewScreen, store, Provider)
Navigation.registerComponent("geovcs.Info", () => Info)

startApp()

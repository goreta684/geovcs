package com.geovcs;

import android.app.Application;

import cl.json.RNSharePackage;
import cl.json.ShareApplication;

import com.facebook.react.ReactApplication;
import net.no_mad.tts.TextToSpeechPackage;
import com.wenkesj.voice.VoicePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.oblador.vectoricons.VectorIconsPackage;
import com.rnfs.RNFSPackage;
import com.reactnativenavigation.NavigationApplication;
import com.artirigo.fileprovider.RNFileProviderPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {

	
	public String getFileProviderAuthority() {
			return "com.geovcs.provider";
	}

	@Override
	public boolean isDebug() {
		// Make sure you are using BuildConfig from your own application
		return BuildConfig.DEBUG;
	}

	protected List<ReactPackage> getPackages() {
		// Add additional packages you require here
		// No need to add RnnPackage and MainReactPackage
		return Arrays.<ReactPackage>asList(
			new VectorIconsPackage(),
			new RNFSPackage(),
			new RNSharePackage(),
			new TextToSpeechPackage(),
			new VoicePackage(),
			new RNFileProviderPackage()
		);
	}

	@Override
	public List<ReactPackage> createAdditionalReactPackages() {
		return getPackages();
  }
  
  @Override
  public String getJSMainModuleName() {
    return "index";
  }
}
export const validateProjectName = (projectName) => {
    let isValid = true

    if(projectName === '') {
        isValid = false
    }
    else {
        isValid = isValid && !projectName.includes('.') && !projectName.includes('@') && !(projectName.length > 30)
    }

    return isValid;
}

export const validatePointNumber = (pointNumber) => {
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    for(let char of pointNumber) {
        if (!numbers.includes(char)) {
            return false;
        }
    }

    return true;
}
export const validateLayerName = (value) => {
//ode triba dodat i ostale dijelove validacije iz LayerConfigScreen-a
    value = value.toUpperCase();
        for (let i = 0; i < value.length; i++) 
            if (!(value[i] >= "A" && value[i] <= "Z" ) && value[i]!=='_' && value[i]!=='-' && 
            value[i]!='0' && 
            value[i]!='1' && 
            value[i]!='2' && 
            value[i]!='3' && 
            value[i]!='4' && 
            value[i]!='5' && 
            value[i]!='6' && 
            value[i]!='7' && 
            value[i]!='8' && 
            value[i]!='9') return false;

            if(value.length > 64) return false;

        return true;
}
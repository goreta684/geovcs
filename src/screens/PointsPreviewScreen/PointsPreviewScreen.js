import React, { Component } from 'react'
import { View, FlatList, StyleSheet, Text, ActivityIndicator, Dimensions } from 'react-native'
import * as RNFS from 'react-native-fs'

class PointsPreviewScreen extends Component {
    state = {
        points: null
    }

    componentDidMount() {
        let projectFilePoints = [];
        RNFS.readFile(RNFS.ExternalDirectoryPath + "/Projects/" + this.props.projectName + ".txt", "utf8")
            .then((response)=>{
                const points = response.split("\n");
                projectFilePoints = points.map(point => {
                    const splitedPoint = point.split(" ")
                    return {
                        pointNumber: splitedPoint[0],
                        layer: splitedPoint[1],
                        layerView: splitedPoint[2],
                        code: splitedPoint[3]
                    }
                })
                console.log("project file points: ",projectFilePoints)
                this.setState({
                    points: projectFilePoints
                })
            
        }).catch(error => console.log(error))
    }

    componentWillUnmount() {
        this.props.enableProjectControls()
    }
    render() {
        return (
            <View style = {styles.mainContainer}>
                <View style = {styles.headerPointsContainer}>
                    <Text style = {styles.oneItemHeader}>Point Number</Text>
                    <Text style = {styles.oneItemHeader}>Layer Number</Text>
                    <Text style = {styles.oneItemHeader}>Layer View</Text>
                    <Text style = {styles.oneItemHeader}>Code</Text>
                </View>
                <View style={styles.pointsContainer}>
                {this.state.points !== null ? (
                    <FlatList
                        style={styles.pointsList}
                        data={this.state.points}
                        renderItem={(point) => (
                            <View style={styles.point}>
                                <Text style = {styles.oneItemPoint}>{point.item.pointNumber}</Text>
                                <Text style = {styles.oneItemPoint}>{point.item.layer}</Text>
                                <Text style = {styles.oneItemPoint}>{point.item.layerView}</Text>
                                <Text style = {styles.oneItemPoint}>{point.item.code}</Text>
                            </View>
                        )} />
                ) : (
                    <ActivityIndicator />
                )}
                </View>

            </View>
            
        )
    }
} 

const styles = StyleSheet.create({
    mainContainer:{
        flex: 1,
        margin: 5
    },
    pointsContainer: {
        height: "90%",
        width: "99%",
    },
    point: {
        margin: 2,
        flexDirection: 'row',
        textAlign: 'center',
        backgroundColor: "#f2f0eb",
    },
    oneItemPoint: {
        flex: 1,
        fontSize: Dimensions.get('window').height * 0.03,
        color: "black"
    },
    headerPointsContainer:{
        height: "10%",
        width: "99%",
        backgroundColor: "#c9f3ff",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    oneItemHeader:{
        flex: 1,
        fontSize: Dimensions.get('window').height * 0.025,
        fontWeight: "bold",
        color: "black",
    }
})

export default PointsPreviewScreen;
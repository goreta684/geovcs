import React, { Component } from 'react'
import { 
    View, 
    Text, 
    Dimensions, 
    TextInput, 
    StyleSheet, 
    FlatList, 
    TouchableOpacity, 
    Picker, 
    CheckBox, 
    ImageBackground, 
    Alert, 
    ActivityIndicator,
    ScrollView } from 'react-native'
import * as RNFS from 'react-native-fs'
import { connect } from 'react-redux';
import Tts from 'react-native-tts'
import Voice from 'react-native-voice'

import startApp from '../StartApp/StartApp'
import imgBack from '../../assets/GeoVCS_background.jpg'
import { validatePointNumber } from '../../utils/validationHelper'
import Icon from 'react-native-vector-icons/Ionicons'

Tts.setDefaultLanguage('en-IE');
Tts.setDefaultRate(0.5)

const NUM_OF_POINTS = 50;

class CodeScreen extends Component {
    state = {
        pointNumberDisable: false,
        layerDisable: false,
        layerViewDisable: false,
        codeDisable: false,

        pointNumber: '',
        layer: '',
        layerView: '',
        code: '',

        points: [],
        previousPoints: [],
        reversedPoints: [],
        newCodeTitle: "New Code",
        newCodeColor: "green",
        codeRevertable: false,

        prevCode: "",
        prevPointNumber: "",
        prevLayerNumber: "",

        layerHandlerValue: "",

        LTyC: "", //LayerTypeCurrent
        LNuC: "", //LayerNumberCurrent
        LNaC: "", //LayerNameCurrent

        loading: false,
        voiceModeOn: false,
        voiceModeTextActive: "Voice is ACTIVATED",
        voiceModeTextDeactive: "Voice is DEACTIVATED",
        onFocusPointNumber: false,
        onFocusLayer: false,
        onFocusLayerView: false,
        onFocusCode: false,
        voiceAlerts: [],
        results: [],
        cursorPoint: false,
        nameOfTextInput: "",
        isLayerDefined: true, //služi da vidimo da li je layer predefiniran, ako je false onda je predefiniran (to je zbog atributa editable u layerView koji je disablean kad je false)
        isLayer99: true
    };

    constructor(props) {
        super(props)
        Voice.onSpeechResults = this.onSpeechResults.bind(this)
        Voice.onSpeechEnd = this.onSpeechEnd.bind(this)
        Voice.onSpeechError = this.onSpeechError.bind(this)
    }

    // variable to hold the references of the textfields
    inputs = {};

    // function to focus the field
    focusTheField = (id) => {
        try {
            this.inputs[id].focus();
        } catch (error) {
            
        }
        
    }

    voiceModeHandler = async () => {
        await this.setState((prevState) => {
            return {
                voiceModeOn: !prevState.voiceModeOn
            }
        })
        
        if (this.state.voiceModeOn) {
            await Voice.start('en-US')
        }
        else {
            await Voice.destroy().then(Voice.removeAllListeners)
            this.setState({
                voiceModeOn: false
            })
        }
    }

    onFocusPointNumberHandler = async () => {
        this.setState({
            onFocusPointNumber: true,
            onFocusLayer: false,
            onFocusLayerView: false,
            onFocusCode: false,
        })
    }

    onFocusLayerHandler = async () => {
        this.setState({
            onFocusLayer: true,
            onFocusPointNumber: false,
            onFocusLayerView: false,
            onFocusCode: false,
        })
    }

    onFocusLayerViewHandler = async () => {
        this.setState({
            onFocusLayerView: true,
            onFocusPointNumber: false,
            onFocusLayer: false,
            onFocusCode: false,
        })
    }

    /*onFocusCodeHandler = async () => {
        this.setState({
            onFocusCode: true,
            onFocusLayerView: false,
            onFocusPointNumber: false,
            onFocusLayer: false
        })
    }*/
    

    onSpeechEnd = async (event) => {
        console.log(event)
    }

    onSpeechResults = async (event) => {
        //let results = event.value;
        this.setState({
            results: event.value,
            voiceAlerts: event.value
        })
        let results = this.state.results;
        let saveWords = ['save', "scene", "seen", "seem", "Caen", "seemed", "Seaham", "same", "see you", "cu", "I see you", "c u", "seeu", "sieve", "Steve", "c", "saved", "sorry", "saree", "Sari", "see"]
        let endCodeWords = ['end', "and", "send", "ende"]
        let resetWords = ["reset", "recette", "ricette", "receipt", "recept", "recipe", "receipt", "illicit", "Ossett", "recess", "reset", "Reese's", "researchers", "research"]
        const newCodeResult = results.find((res) => res === 'new' || res === 'code' || res === 'new code' || res === 'new coat' || res === 'new gold' || res === 'mute' || res === 'music' || res === 'mu' || res === 'knew' || res === 'new coal' || res === 'new Cole' )
        let numbers = ['0','1','2','3','4','5','6','7','8','9'];
        let pointWords = ["point", "PT", "Pointe", "joined", "poynt", "boy", "coin", "join", "points", "appoint"]
        let layerWords = ["Leia", "layer", "laya", "layher", "later", "Leia", "lair", "liar"]
        let viewWords = ["Vue", "view", "you", "review", "UL"]
        let offWords = ["off", "cough","shutdown", "Showdown"]
        let BLWords = ["beginlight", "beginline", "beginlife", "beginlights", "beginninglight", "beginningline", "beginlive", "beginlight", "beginLyne"]
        let ELWords = ["andlife", "andlight", "landline", "andwhy", "andline","endline", "landline"]
        let IDWords = ["ID", "Heidi", "eide", "idee", "eide"]

        if (newCodeResult) {
            this.newCodeHandler()
            await Voice.stop();
            this.setState({
                onFocusPointNumber: true,
                cursorPoint: true
            })
        }
        results[0] = results[0].replace(/ /g, "");//remove whitespaces inside a string

        if(this.state.onFocusPointNumber){
            this.setState({
                nameOfTextInput: "ID"
            })
            let bln = true;
            for (c of results[0]){
                if(!numbers.includes(c) || results[0][0] === '0' || results[0][0] === 0){
                    bln = false;
                }
            }
            if(bln){
                this.setState({
                    pointNumber: results[0],
                    layerDisable: true,
                    onFocusPointNumber: false,
                })
                Tts.speak("You say "+ this.state.pointNumber + "it is correct?")
                
            }
            this.setState({
                onFocusPointNumber: false
            })
            
        }
        /*
        this.setState({
                        pointNumber: results[0],
                        layerDisable: true,
                        //onFocusPointNumber: false
                    })
        */
        if(this.state.onFocusLayer){

            let thisLayer = "";
            this.setState({
                nameOfTextInput: "layer",
                isLayerDefined: true,
                isLayer99: true
            })
            let bln = true;
            for (c of results[0]){
                if(!numbers.includes(c) || results[0][0] === '0' || results[0][0] === 0){
                    bln = false;
                }
            }
            if(bln){
                thisLayer = results[0];
                this.setState({
                    layer: results[0],
                    layerViewDisable: true,
                })
                if(results[0]==="99"){
                    this.setState({
                        isLayer99: false
                    })
                }
                Tts.speak("You say "+ this.state.layer + "it is correct?")
            }

            for(let i=0;i<this.props.createdLayers.length;i++){
                if(this.props.createdLayers[i].layerNumber === thisLayer || thisLayer === "99"){
                    this.setState({
                        isLayerDefined: false//ako je definiran onda ćemo ugasiti view input za pisanje
                    })
                    break
                    
                }
            }
            
        }

        if(this.state.onFocusLayerView){
            if(this.state.isLayerDefined){
                this.setState({
                    nameOfTextInput: "view"
                })
                let bln = true;
                for (c of results[0]){
                    if(c === ' '){
                        bln = false
                    }
                }
                if(bln && results[0] !== "off" && results[0] !== "save" && results[0] !== "end" && results[0] !== "and" && results[0] !== "new" && results[0] !== "code" && results[0] !== "newcode" && results[0] !== "reset"){//dodati i ostale ključne riječi uz off
                    this.setState({
                        layerView: results[0],
                        codeDisable: true,
                    })
                    Tts.speak("You say "+ this.state.layerView + "it is correct?")
                }
            }
            
            
        }
        if(saveWords.includes(results[0])){
            this.saveCodeHandler()
           
        }
        if(endCodeWords.includes(results[0])){
            this.endCodingHandler()
            
        }

        if(resetWords.includes(results[0])){
            this.resetCodingHandler()
            
        }

        if(IDWords.includes(results[0])){
            try {
                this.inputs["pointField"].focus();
                
    
            } catch (error) {
                
            }
        }

        if(layerWords.includes(results[0])){
            try {
                this.inputs["layerField"].focus();
                
    
            } catch (error) {
                
            }
        }

        if(viewWords.includes(results[0])){
            try {
                this.inputs["viewField"].focus();
                
    
            } catch (error) {
                
            }
        }

        if(offWords.includes(results[0])){
            this.voiceModeHandler(),
            this.resetCodingHandler()
        }

        if(BLWords.includes(results[0])){
            this.setState({
                code: "BL"
            })
        }

        if(ELWords.includes(results[0])){
            this.setState({
                code: "EL"
            })
        }

        if(pointWords.includes(results[0])){
            this.setState({
                code: "P"
            })
        }
        // for ID
        if(results[0]==="yes" && this.state.nameOfTextInput === "ID"){
            try {
                this.inputs["layerField"].focus();
            } catch (error) {
            }
        }

        if(results[0]==="no" && this.state.nameOfTextInput === "ID"){
            try {
                this.inputs["layerField"].focus();
            } catch (error) {
            }

            try {
                this.inputs["pointField"].focus();
            } catch (error) {
            }
        }

        //for layer
        if(results[0]==="yes" && this.state.nameOfTextInput === "layer"){
            try {
                this.inputs["viewField"].focus();
            } catch (error) {
            }

            if(!this.state.isLayerDefined && this.state.isLayer99 === true){
                Tts.speak("Say code number")
            }
        }

        if(results[0]==="no" && this.state.nameOfTextInput === "layer"){
            try {
                this.inputs["pointField"].focus();
            } catch (error) {
            }
            try {
                this.inputs["layerField"].focus();
            } catch (error) {
            }
        }

        //for view
        if(results[0]==="yes" && this.state.nameOfTextInput === "view"){
            try {
                Tts.speak("Choose code number!")
            } catch (error) {
            }
        }

        if(results[0]==="no" && this.state.nameOfTextInput === "view"){
            try {
                this.inputs["layerField"].focus();
            } catch (error) {
            }
            try {
                this.inputs["viewField"].focus();
            } catch (error) {
            }
        }

        this.setState({
            onFocusLayerView: false,
            onFocusPointNumber: false,
            onFocusLayer: false,
            onFocusCode: false,
        })

        console.log(results);
        await Voice.start('en-US')
    }

    onSpeechError = async (event) => {
        await Voice.destroy().then(Voice.removeAllListeners)
        await Voice.start('en-US')
    }

    pointNumberHandler = (value) => {
        this.setState({
            pointNumber: value,
            layerDisable: true,
        })
    }

    checkLayerHandler = () => {
        let layer_number_current = this.state.layer
        this.setState({
            LNuC: layer_number_current
        })
        let layer_type_current = ""
        let layer_name_current = ""
        let index = false

        let CREATED_LAYERS_LENGTH = this.props.createdLayers.length
        let POINTS_LEN = this.state.points.length


        //prolazimo po nizu LAYERA
        for(let i =0; i<CREATED_LAYERS_LENGTH; i++){
            if(this.props.createdLayers[i].layerNumber === layer_number_current){
                layer_type_current = this.props.createdLayers[i].layerType
                layer_name_current = this.props.createdLayers[i].layerName
                index = true;
                this.setState({
                    LTyC: layer_type_current,
                    LNaC: layer_name_current
                })
            }
        }

        //ako ima bar 1 točka && zadnja je BL && layer je predefiniran && type mu je line
        if(POINTS_LEN > 0 && this.state.points[POINTS_LEN-1].code === "BL" && index === true && layer_type_current === "line"){
            this.setState({
                layerView: layer_name_current,
                codeDisable: true,
                code: "EL",
                isLayerDefined: false
            })
        }

         //ako je unešen layer number 99
        else if (layer_number_current === "99"){
                this.setState({
                    layerView: "XX",
                    codeDisable: true,
                    code: "P",
                    isLayerDefined: false,
                    isLayer99: false
                })
            }

        //ako layerNumber nije predefiniran u layer configu
        else if (index === false) { 
            this.setState({
                codeDisable: true,
                code: "BL",
                layerView:""
            })
        }
        //ako unosimo predefiniranu točku
        else if (layer_type_current === "point" && index === true) {
            this.setState({
                layerView: layer_name_current,
                codeDisable: true,
                code: "P",
                isLayerDefined: false
            })
        }
        //ako unosimo predefinirani layer number koji je line
        else if (layer_type_current === "line" && index === true){
            this.setState({
                layerView: layer_name_current,
                codeDisable: true,
                code: "BL",
                isLayerDefined: false
            })
        }
    }

    layerHandler = (value) => {
        this.setState({
            layer: value,
            layerViewDisable: true,
            isLayerDefined: true,
            isLayer99: true
        })
    }

    layerViewHandler = (value) => {
        this.setState({
            layerView: value,
        })
    }

    codeHandler = (value) => {
        this.setState({
            code: value
        })
    }

    checkPointNumber = (value) => {
        
        let POINTS_LEN = this.state.points.length;
        if(POINTS_LEN > 0){
            if (Number(value) <= Number(this.state.points[POINTS_LEN-1].pointNumber)){
                return false;
            }
            if (value.includes(" ") || value.includes(",") || value.includes(".") || value.includes("-") || value ==="" ) {
                return false;
            }
            if (Number(value) >= 100000){
                return false;
            }
        }else{
            if (value.includes(" ") || value.includes(",") || value.includes(".") || value.includes("-") || value ==="" ) {
                return false;
            }
            if (Number(value) >= 100000){
                return false;
            }
        }
        return true
    }

    checkLayerView = (value) => {
        if (value.includes(" ") || value.includes(",") || value.includes(".") || value.includes("-") || value ==="") {
            return false
        }
        return true
    }

    checkLayer = (value) => {
        if (value.includes(" ") || value.includes(",") || value.includes(".") || value.includes("-") || value ==="") {
            return false
        }
        return true
    }

    saveCodeHandler = async () => {
        /////////////////////////////////////////
        await this.setState((prevState) => {
            return {
                newCodeTitle: "New Code",
                newCodeColor: "green",
                pointNumberDisable: false,
                layerDisable: false,
                layerViewDisable: false,
                codeDisable: false,
                previousPoints: prevState.points,
                codeRevertable: true,
                isLayerDefined: true,
                isLayer99: true
            }
        })

        let POINTS_LEN = this.state.points.length
        let POINTS_LEN_COUNTER = POINTS_LEN
        let code_current = this.state.code
        let pointNumber_current = this.state.pointNumber
        let layer_number_current = this.state.layer
        let layer_name_current = this.state.layerView
        
        if (this.checkPointNumber(this.state.pointNumber) && this.checkLayer(this.state.layer) && this.checkLayerView(this.state.layerView) && (POINTS_LEN > 0 && POINTS_LEN < NUM_OF_POINTS)) {
            let code_prev = this.state.points[POINTS_LEN-1].code//
            let pointNumber_prev = this.state.points[POINTS_LEN-1].pointNumber //
            let layer_number_prev = this.state.points[POINTS_LEN-1].layer//
            let layer_name_prev = this.state.points[POINTS_LEN-1].layerView
            
            if(code_prev === "BL" && code_current === "BL"){//------------------------------------------
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)

                if (finish-start === 1){
                    await this.savePointsToState(finish, layer_number_prev, layer_name_prev, "EL")
                    this.updateFilePoints(this.state.points)
    
                    await this.savePointsToState(finish + 1, layer_number_current, layer_name_current, "BL")
                    this.updateFilePoints(this.state.points)
                }
                else{
                    for (let j = start + 1; j < finish-1; j++) {
                        await this.savePointsToState(Number(j), layer_number_prev, layer_name_prev, "L")   
                    }
                    this.updateFilePoints(this.state.points)
    
                    await this.savePointsToState(pointNumber_current-1, layer_number_prev, layer_name_prev, "EL")
                    this.updateFilePoints(this.state.points)
                      
                    await this.savePointsToState(pointNumber_current, layer_number_current, layer_name_current, "BL")
                    this.updateFilePoints(this.state.points)
                }
            }
            else if(code_prev === "EL" && layer_number_current === "99"){
                ////////////////////////////////////////////////////////////
                await this.savePointsToState(pointNumber_current, this.state.layer, layer_name_current, code_current)
                this.updateFilePoints(this.state.points)
            }
            else if((code_prev === "EL" && code_current === "P") || (code_prev === "EL" && this.state.LTyC === "point")){
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)

                for (let j = start + 1; j < finish+1; j++) {
                    await this.savePointsToState(Number(j), this.state.layer, layer_name_current, "P")
                }
                this.updateFilePoints(this.state.points)
            }
            else if((code_prev === "BL" && this.state.LTyC ==="point") || (code_prev === "BL" && code_current === "P")){//------------------------------------------
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)
                if (finish-start === 1){
                    await this.savePointsToState(finish, layer_number_prev, layer_name_prev, "EL")
                    this.updateFilePoints(this.state.points)
    
                    await this.savePointsToState(finish + 1, layer_number_current, layer_name_current, "P")
                    this.updateFilePoints(this.state.points)
                }
                else{
                    for (let j = start + 1; j < finish-1; j++) {
                        await this.savePointsToState(j, layer_number_prev, layer_name_prev, "L")   
                    }
                    this.updateFilePoints(this.state.points)
    
                    await this.savePointsToState(pointNumber_current-1, layer_number_prev, layer_name_prev, "EL")
                    this.updateFilePoints(this.state.points)
    
                    await this.savePointsToState(pointNumber_current, layer_number_current, layer_name_current, "P")
                    this.updateFilePoints(this.state.points)
                }
            }
            else if(code_prev === "P" && layer_number_current === "99"){
                ////////////////////////////////////////////////////////////
                await this.savePointsToState(pointNumber_current, this.state.layer, layer_name_current, code_current)
                this.updateFilePoints(this.state.points)
            }
            else if(layer_number_prev === "99"){
                ////////////////////////////////////////////////////////////
                await this.savePointsToState(pointNumber_current, this.state.layer, layer_name_current, code_current)
                this.updateFilePoints(this.state.points)
            }
            else if(code_current === "BL" && code_prev === "P"){
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)
                let counter = 0

                if (finish-start === 1){
                    await this.savePointsToState(finish, this.state.layer, layer_name_current, "BL")
                    this.updateFilePoints(this.state.points)

                }
                else{
                    for (let j = start + 1; j < finish; j++) {
                        counter = j + 1
                        await this.savePointsToState(j, layer_number_prev, layer_name_prev, "P")  
                    }
                    this.updateFilePoints(this.state.points)
                    await this.savePointsToState(counter, this.state.layer, layer_name_current, "BL")
                    this.updateFilePoints(this.state.points)
                }
            }
            else if (code_current === "BL") {//------------------------------------------
                await this.savePointsToState(pointNumber_current, this.state.layer, layer_name_current, code_current)
                this.updateFilePoints(this.state.points)
            }
            else if (code_current === "EL" && code_prev === "BL") {//------------------------------------------
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)
                let counter = 0

                if (finish-start === 1){
                    await this.savePointsToState(finish, layer_number_prev, layer_name_prev, "EL")
                    this.updateFilePoints(this.state.points)

                }
                else{
                    for (let j = start + 1; j < finish; j++) {
                        counter = j + 1
                        await this.savePointsToState(j, layer_number_prev, layer_name_prev, "L")
                    }
                    this.updateFilePoints(this.state.points)

                    await this.savePointsToState(counter, layer_number_prev, layer_name_prev, "EL")
                    this.updateFilePoints(this.state.points)
                }
            }
            else if (code_prev === "P" && code_current === "P" && this.state.layer !== layer_number_prev){//------------------------------------------
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)
                let counter = 0

                if(finish-start === 1){
                    await this.savePointsToState(finish, layer_number_current, layer_name_current, "P")
                    this.updateFilePoints(this.state.points)
                }
                else {
                    for (let j = start + 1; j < finish; j++) {
                        counter = j + 1
                        await this.savePointsToState(j, layer_number_prev, layer_name_prev, "P")
                    }
                    this.updateFilePoints(this.state.points)

                    await this.savePointsToState(counter, layer_number_current, layer_name_current, "P")
                    this.updateFilePoints(this.state.points)

                }
            }
            else if(code_prev === "P" || this.state.LTyC === "point" || code_current === "P") {
                let start = Number(pointNumber_prev)
                let finish = Number(pointNumber_current)
                let counter = 0

                if(finish-start === 1){
                    await this.savePointsToState(finish, layer_number_current, layer_name_current, "P")
                    this.updateFilePoints(this.state.points)
                }
                else {
                    for (let j = start + 1; j < finish; j++) {
                        counter = j + 1
                        await this.savePointsToState(j, layer_number_prev, layer_name_prev, "P")
                    }
                    this.updateFilePoints(this.state.points)

                    await this.savePointsToState(counter, layer_number_current, layer_name_current, "P")
                    this.updateFilePoints(this.state.points)
                }
            }
            if (this.state.voiceModeOn) {
                Tts.speak('Saved');
            }
            else {
                alert("New code saved!")
            }
            
    }
    else if (POINTS_LEN === 0){
        if (this.checkPointNumber(this.state.pointNumber) && this.checkLayer(this.state.layer) && this.checkLayerView(this.state.layerView) && POINTS_LEN === 0){
            let codeCurrent = "BL";
            if(this.state.LTyC === "point"){
                codeCurrent = "P";
            }
            else if(layer_number_current === "99"){
                codeCurrent = "P"
            }
            else if(code_current === "P"){
                codeCurrent = "P"
            }
            await this.savePointsToState(pointNumber_current, layer_number_current, layer_name_current, codeCurrent)
            this.updateFilePoints(this.state.points)

            if (this.state.voiceModeOn) {
                Tts.speak('Saved');
            }
            else {
                alert("New code saved!")
            }

        }
        else{
            if (this.state.voiceModeOn) {
                Tts.speak("Something wen't wrong");
            }
            else {
                alert("Point number already exist or some fields are empty!\nPoint number must be between 1 and 100 000.")
            }
            
            this.setState({
                pointNumberDisable: true,
                layerDisable: true,
                layerViewDisable: true,
                codeDisable: true,
                newCodeTitle: "Reset",
                newCodeColor: "red",

            })
        }
    }
    else {
        if (POINTS_LEN >= NUM_OF_POINTS) {
            alert('You reached limit of '+NUM_OF_POINTS+' points in limited version. For more points buy GeoVCSPro version.')
        }
        else {
            if (this.state.voiceModeOn) {
                Tts.speak("Something wen't wrong");
            }
            else {
                alert("Point number already exist or some fields are empty!\nPoint number must be between 1 and 100 000.")
            }
            this.setState({
                pointNumberDisable: true,
                layerDisable: true,
                layerViewDisable: true,
                codeDisable: true,
            })
        }
    }
}

    savePointsToState = async (pointNumber, layer, layerView, code) => {
        const numOfPoints = this.state.points.length;
        if (numOfPoints < NUM_OF_POINTS) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    loading: true,
                    points: prevState.points.concat({
                        pointNumber: pointNumber,
                        layer: layer,
                        layerView: layerView,
                        code: code
                    }),
                }})
        }
    }

    newCodeHandler = () => {
        this.setState({
            pointNumber: '',
            layer: '',
            layerView:'',
            code: '',
            pointNumberDisable: true,
            layerDisable: true,
            newCodeTitle: "Reset",
            newCodeColor: "red"
        })

        try {
            this.inputs["layerField"].focus();

        } catch (error) {
            
        }

        try {
            this.inputs["pointField"].focus();
            
            this.setState({
                pointNumber: ""
            })

        } catch (error) {
            
        }
        
    }

    resetCodingHandler = () => {
        this.setState((prevState) => {
            return {
                ...prevState,
                pointNumber: '',
                layer: '',
                layerView: '',
                code: '',
                pointNumberDisable: false,
                layerViewDisable: false,
                layerDisable: false,
                codeDisable: false,
                newCodeTitle: 'New Code',
                newCodeColor: "green"
            }
        })
    }

    endCodingHandler = () => {
        Alert.alert(
            'Exit from coding',
            'Do you want to exit coding?',
            [
              {text: 'No', onPress: () => {return}, style: 'cancel'},
              {text: 'Yes', onPress: () => startApp()},
            ],
            { cancelable: false });
            return true;
    }

    async componentDidMount() {
        this.setState({loading: true})
        let currentProjectPoints = [];
        await RNFS.readFile(RNFS.ExternalDirectoryPath+"/Projects/"+this.props.currentProject+".txt","utf8")
            .then((response)=>{
                const points = response.split("\n");
                let slicedpoints = points.slice(1);
                if (points[1] === '') {
                    slicedpoints = points.slice(2, points.length)
                }
                
                    currentProjectPoints = slicedpoints.map(point => {
                        const splitedPoint = point.split(" ")
                        return {
                            pointNumber: splitedPoint[0],
                            layer: splitedPoint[1],
                            layerView: splitedPoint[2],
                            code: splitedPoint[3]
                        }
                    })
                
                
            })
           
                this.setState({
                    points: currentProjectPoints,
                    previousPoints: currentProjectPoints,
                    loading: false
                })
            

    }

    updateFilePoints = (updatedPoints) => {
        this.setState({loading: true})
        let writePoints = this.props.currentProject + '\n';
        if(this.state.points.length > 1){
            writePoints += updatedPoints[0].pointNumber + ' ' + updatedPoints[0].layer + ' ' + updatedPoints[0].layerView + ' ' + updatedPoints[0].code + '\n';
        }
        for(let i = 1; i < updatedPoints.length; i++) {
            if (i < updatedPoints.length - 1) {
                writePoints +=  updatedPoints[i].pointNumber + ' ' + updatedPoints[i].layer + ' ' + updatedPoints[i].layerView + ' ' + updatedPoints[i].code + '\n';
            }
            else {
                writePoints +=  updatedPoints[i].pointNumber + ' ' + updatedPoints[i].layer + ' ' + updatedPoints[i].layerView + ' ' + updatedPoints[i].code;
            }
        }

        RNFS.writeFile(RNFS.ExternalDirectoryPath+"/Projects/"+this.props.currentProject+".txt", writePoints, "utf8")
            .then(response => {
                this.setState({loading: false})
            })
            .catch(error => {
                console.log(error)
            })
    }


    alertEditLastCode = async () => {
        Alert.alert(
            'Edit last code',
            'Do you want to edit last code?',
            [
              {text: 'No', onPress: () => {return}, style: 'cancel'},
              {text: 'Yes', onPress: () => 
              this.editLastCode()
            },
            ],
            { cancelable: false });
            return true;
    }

    alertDeleteLastCode = async () => {
        Alert.alert(
            'Delete last code',
            'Do you want to delete last code?',
            [
              {text: 'No', onPress: () => {return}, style: 'cancel'},
              {text: 'Yes', onPress: () => 
              this.deleteLastCode()
            },
            ],
            { cancelable: false });
            return true;
    }

    editLastCode = async () => {
        const pointsLength = this.state.points.length
        const lastCode = this.state.points[pointsLength - 1]
        if(pointsLength > 0){
            try {
                await this.deleteLastCode();
                await this.setState((prevState) => {
                    return {
                        ...prevState,
                        pointNumberDisable: true,
                        layerViewDisable: true,
                        layerDisable: true,
                        codeDisable: true,
                        pointNumber: lastCode.pointNumber,
                        layer: lastCode.layer,
                        layerView: lastCode.layerView,
                        code: lastCode.code
                }
            })
                
            } catch (error) {
        
            }
        }
        else{
            alert("File is empty!")
        }
    }

    deleteLastCode = async () => {
        await this.setState((prevState) => {
            return {
                ...prevState,
                points: prevState.previousPoints
            }
        })
        await this.updateFilePoints(this.state.points)
    }

    componentWillUnmount() {
        this.props.enableButtons();
        Voice.destroy().then(Voice.removeAllListeners);
        this.setState({
            voiceModeOn: false
        })
    }

    render() {
        if (this.state.loading) {
            return (
                <ActivityIndicator size={100} />
            )
        }

        return (
            <ImageBackground 
                source={imgBack}
                style = {styles.imgBackgStyle}>
            <View style = {styles.mainView}>
                <View style = {styles.checkBtnView}>
                    <View style = {styles.checkBoxStyle}>
                        <CheckBox
                            onChange={this.voiceModeHandler}
                            value={this.state.voiceModeOn}/>
                            <Text style = {{color: "white", fontSize: Dimensions.get('window').width * 0.03}}>
                                {this.state.voiceModeOn === false ? "VOICE MODE" : "VOICE MODE"}
                            </Text>
                            {this.state.voiceModeOn === false ? <Icon   
                                size={Dimensions.get('window').height * 0.04}
                                name="md-mic-off"
                                style={{marginLeft: 10}}
                                color="red" /> 
                                : 
                                <Icon   
                                size={Dimensions.get('window').height * 0.04}
                                name="md-mic"
                                style={{marginLeft: 10}}
                                color="green" />}
    
                            
                    </View>
                </View>
                <View style = {styles.newCodeView}>
                    <TouchableOpacity 
                        style={styles.newCodeBtn} 
                        onPress={this.state.newCodeTitle === 'New Code' ? this.newCodeHandler : this.resetCodingHandler}
                        >
                        <View >
                            <Text
                                style={{
                                    backgroundColor: this.state.newCodeColor,
                                    fontWeight: "bold",
                                    fontSize: Dimensions.get('window').height * 0.02,
                                    color: "white",
                                    height: "100%",
                                    width: "100%",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    textAlign: "center",
                                    textAlignVertical: "center"
                                }}
                                >
                                    {this.state.newCodeTitle}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style = {styles.textInputsView}>
                        {this.state.pointNumberDisable ? 
                    <View style={styles.labelAndInput}>
                        <TextInput
                            keyboardType='numeric'
                            placeholder="Point Number"
                            onChangeText={this.pointNumberHandler}
                            style={styles.textInputs}
                            editable={this.state.pointNumberDisable}
                            value={this.state.pointNumber}
                            onFocus={this.onFocusPointNumberHandler}
                            onTouchStart={this.onFocusPointNumberHandler}
                            
                            label={"Point"}
                            blurOnSubmit={ false }
                            returnKeyType={ 'next' }
                            onSubmitEditing={() => { this.focusTheField('layerField'); }}
                            ref={input => { this.inputs['pointField'] = input }}
                        />
                    </View> : null}
                        {this.state.layerDisable ? 
                    <View style={styles.labelAndInput}>
                        <TextInput
                            keyboardType='numeric'
                            placeholder="Layer"
                            onChangeText={this.layerHandler}
                            onEndEditing = {this.checkLayerHandler}
                            style={styles.textInputs}
                            editable={this.state.layerDisable}
                            value={this.state.layer} 
                            onFocus={this.onFocusLayerHandler}
                            onTouchStart={this.onFocusLayerHandler}
                            
                            label={"Layer"}
                            ref={input => { this.inputs['layerField'] = input }}
                            blurOnSubmit={ false }
                            returnKeyType={ 'next' }
                            onSubmitEditing={() => { this.focusTheField('viewField'); }}
                            
                            />
                    </View> : null}
                        {this.state.layerViewDisable ? 
                    <View style={styles.labelAndInput}>
                        <TextInput
                            style={styles.textInputs}
                            placeholder="Layer View"
                            onChangeText={this.layerViewHandler}
                            value={this.state.layerView}
                            editable={this.state.layerViewDisable}
                            onFocus={this.onFocusLayerViewHandler}
                            onTouchStart={this.onFocusLayerViewHandler}
                            editable={this.state.isLayerDefined}

                            label={"View"}
                            ref={input => { this.inputs['viewField'] = input }}
                            />
                    </View> : null}
                        {this.state.codeDisable ? 
                    <View style={styles.labelAndInput}>
                        <View style = {{backgroundColor:"white", flex: 1}}>
                            <Picker 
                                selectedValue={this.state.code}
                                onValueChange={this.codeHandler}
                                enabled={this.state.isLayer99}>
                                    <Picker.Item label="Begin Line" value="BL" />
                                    <Picker.Item label="End Line" value="EL" />
                                    <Picker.Item label="Point" value="P" />
                            </Picker>
                        </View>
                    </View> : null}
                </View>
                <View style = {styles.saveCodeView}>
                    <TouchableOpacity disabled={!this.state.pointNumberDisable} style = {{opacity: 0.9, width: "100%"}}>
                        <Text style = {styles.btnForm} onPress={() => {this.saveCodeHandler()}}>
                            Save Code  
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style = {styles.pointsAndButtons}>
                    <View style={styles.code}>
                        <FlatList
                            inverted
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.points.slice(this.state.points.length >= 50 ? this.state.points.length - 50 : 0, this.state.points.length).reverse()} 
                            renderItem={(point) => {
                                const pointsLength = this.state.points.length
                                return (
                                    <View key={point.index} style={{backgroundColor: (point.item.pointNumber === this.state.points[pointsLength - 1].pointNumber) ? '#d0f7d0' : ''}}>
                                        <Text style = {styles.flatListPoints}>{point.item.pointNumber + ' ' + point.item.layer + ' ' + point.item.layerView + ' ' + point.item.code}</Text>
                                    </View>
                            )}}
                        />
                    </View>
                    <View style = {styles.editButtons}>
                        <TouchableOpacity disabled={!this.state.codeRevertable} style = {{opacity: 0.9, width: "100%"}} onPress={this.alertEditLastCode}>
                            <View style = {styles.codeBtnForm}>
                                <Text style = {styles.codeBtnText}>
                                    Edit last code  
                                </Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity disabled={!this.state.codeRevertable} style = {{opacity: 0.9, width: "100%"}} onPress={this.alertDeleteLastCode}>
                            <View style = {styles.codeBtnForm}>
                                <Text style = {styles.codeBtnText}>
                                    Delete last code  
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style = {styles.endCodingView}>
                    <TouchableOpacity style = {{opacity: 0.9, width: "100%"}}>
                        <Text 
                            style = {styles.btnForm} 
                            onPress={() => {this.endCodingHandler()}}
                            >
                            End Coding  
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    imgBackgStyle:{
        height: Dimensions.get('window').height
    },
    mainView:{
        flex: 1, 
        alignItems: "center", 
    },
    checkBtnView:{
        width: "95%", height: "4%", 
        margin: 2, 
        marginBottom: 2,
        flexDirection: "row",
    },
    checkBoxStyle:{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        paddingLeft: 1,
    },
    newCodeView: {
        width: "95%", 
        height: "6%", 
        alignItems:"center", 
    },
    newCodeBtn:{
        width:"100%",
    },
    inputs: {
        width: "95%",
        alignItems: "center"
    },
    code: {
        width: "60%",
        borderWidth: Dimensions.get('window').width / 70,
        borderColor: "black",
        height: "100%",
        backgroundColor: 'white'
    },
    textInputsView:{
        width: "95%", 
        height: "30%", 
        marginTop: 2, 
        marginBottom: 2, 
    },
    labelAndInput: {
        flex: 1,
        borderWidth: 1,
        margin: 1,
    },
    textInputs: {
        fontSize: Dimensions.get('window').height /60,
        fontWeight: "bold",
        width: "100%",
        flex: 1,
        backgroundColor:"white",
    },
    saveCodeView:{width: "95%", height: "7%", alignItems:"center"},
    pointsAndButtons:{
        width: "95%", 
        height: "20%", 
        flexDirection: "row", 
        justifyContent: "space-around", 
        padding: 1, 
        marginTop: 1, 
        marginBottom: 1,
    },
    endCodingView:{
        width: "95%", 
        height: "7%", 
        alignItems: "center", 
        paddingTop: 1, 
        paddingBottom: 1,
        //marginBottom: 10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    editButtons:{
        width: "39%",
        justifyContent:"space-around",
    },
    codeBtnForm:{
        borderWidth: 3,
        borderColor: "white",
        height: Dimensions.get('window').height * 0.06,
        backgroundColor: "yellow",
        alignItems: "center",
        justifyContent: "center"
    },
    btnForm: {
        backgroundColor: '#fa6000',
        fontSize: Dimensions.get('window').height * 0.028,
        fontWeight: "bold",
        textAlign: "center",
        textAlignVertical: "center",
        color: 'white',
        width: "100%",
        height: "100%",
        borderWidth: 3,
        borderColor: "white",
        borderRadius: 10   
    },
    codeBtnText:{
        color: "black",
        fontSize: Dimensions.get('window').height * 0.017, 
        fontWeight: "bold"
    },
    flatListPoints:{
        color: "black"
    },
    touchStyle:{
        fontSize: Dimensions.get('window').height /60,
        fontWeight: "bold",
        width: "100%",
        flex: 1,
        backgroundColor:"white",
    }
})

const mapStateToProps = (state) => {
    return {
        currentLayer: state.layer.currentLayer,
        createdLayers: state.layer.layers,
        currentProject: state.project.currentProject
    }
}

export default connect(mapStateToProps)(CodeScreen)
import React, { Component, useEffect } from 'react'
import { 
    View, 
    StyleSheet, 
    Text, 
    TouchableOpacity,
    Alert, 
    BackHandler, 
    Dimensions
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'

import { fetchProjects, setProjectMode, fetchLayers } from '../../store/actions/index'

class StartScreen extends Component {
    state = {
        createProjectDisabled: false,
        openProjectDisabled: false,
        manageProjectDisabled: false,
        layerConfigDisabled: false,
        startCodingDisabled: false,
        infoButtonDisabled: false
    }
    
    constructor(props) {
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent)
    }

    onNavigatorEvent = event => {
        if(event.type === "NavBarButtonPress") {
            if(event.id === "infoButton") {
                this.setState({
                    infoButtonDisabled: true
                })
                this.props.navigator.push({
                    screen: 'geovcs.Info',
                    title: 'Help',
                    passProps: {
                        enableButtons: this.enableButtons
                    }
                })
            }
        }
    }

    static navigatorStyle = {
        navBarButtonColor: "#1463ff",
        navBarButtonSize: 30
    }

    componentDidMount() {
        this.props.getAllProjects();
        this.props.getAllLayers();
    }

    enableButtons = () => {
        this.setState({
            createProjectDisabled: false,
            openProjectDisabled: false,
            manageProjectDisabled: false,
            layerConfigDisabled: false,
            startCodingDisabled: false,
            infoButtonDisabled: false
        })
    }

    createNewProjectHandler = () => {
        this.setState({
            createProjectDisabled: true
        })
        if (this.props.allProjects.length >= 1) {
            this.enableButtons()
            alert('In limited version you can create only one projects. For more projects buy GeoVCSPro version.')
        }
        else {
            this.props.navigator.push({
                screen: "geovcs.CreateProjectScreen",
                title: "Create New Project",
                passProps: {
                    enableButtons: this.enableButtons
                }
            })
        }
    }

    openProjectHandler = () => {
        this.setState({
            openProjectDisabled: true
        })
        this.props.changeProjectMode('open')
        this.props.navigator.push({
            screen: "geovcs.ProjectsScreen",
            title: "Open existing project",
            passProps: {
                enableButtons: this.enableButtons
            }
        })
    }

    manageProjectHandler = () => {
        this.setState({
            manageProjectDisabled: true
        })
        this.props.changeProjectMode('manage')
        this.props.navigator.push({
            screen: "geovcs.ProjectsScreen",
            title: "Manage project",
            passProps: {
                enableButtons: this.enableButtons
            }
        })
    }

    startCodingHandler = () => {
        this.setState({
            startCodingDisabled: true
        })
        if(this.props.currentProject !== ""){
            this.props.navigator.push({
                screen: "geovcs.CodeScreen",
                title: this.props.currentProject,
                passProps: {
                    enableButtons: this.enableButtons
                }
            })
            Alert.alert(
                'Attention',
                'Current project is '+ this.props.currentProject,
                [
                  {text: 'OK', 
                    onPress: () =>{}
                },
                ],
                {cancelable: false},
              );
        }
        else{
            Alert.alert(
                'Warning',
                'Please select a project!',
                [
                  {text: 'OK', 
                    onPress: () =>{[this.props.changeProjectMode('open'),this.props.navigator.push
                    (
                        {
                            screen: "geovcs.ProjectsScreen",
                            title: "Open existing project",
                            passProps: {
                                enableButtons: this.enableButtons
                            }
                        }
                    )]}
                },
                ],
                {cancelable: false},
              );
        }
    }

    layerConfigHandler = () => {
        this.setState({
            layerConfigDisabled: true
        })
        this.props.navigator.push({
            screen: "geovcs.LayerConfigScreen",
            title: "Layer Config",
            passProps: {
                enableButtons: this.enableButtons
            }
        })
    }

    exitAppHandler = () =>{
        Alert.alert(
            'Exit App',
            'Do you want to exit?',
            [
              {text: 'No', onPress: () => {return}, style: 'cancel'},
              {text: 'Yes', onPress: () => BackHandler.exitApp()},
            ],
            { cancelable: false });
            return true;
    }

    render() {
        return(
            <View style = {styles.mainView}>
                <View style = {styles.oneButtonView}>
                    <TouchableOpacity style = {styles.touchableStyle}
                        disabled={this.state.createProjectDisabled}
                        onPress={this.createNewProjectHandler}
                        >
                        <Text style = {styles.btnText}>
                            New Project
                        </Text>
                        <Icon
                            size={Dimensions.get('window').height * 0.04}
                            name="md-arrow-dropright"
                            color="#FFFFFF"
                            style = {styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>

                <View style = {styles.oneButtonView}>
                    <TouchableOpacity style = {styles.touchableStyle}
                        disabled={this.state.openProjectDisabled}
                        onPress={this.openProjectHandler}
                        >
                        <Text style = {styles.btnText}>
                            Open Project
                        </Text>
                        <Icon
                            size={Dimensions.get('window').height * 0.04}
                            name="md-arrow-dropright"
                            color="#FFFFFF"
                            style = {styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>

                <View style = {styles.oneButtonView}>
                    <TouchableOpacity style = {styles.touchableStyle}
                        disabled={this.state.manageProjectDisabled}
                        onPress={this.manageProjectHandler}
                        >
                        <Text style = {styles.btnText}>
                            Manage Project
                        </Text>
                        <Icon
                            size={Dimensions.get('window').height * 0.04}
                            name="md-arrow-dropright"
                            color="#FFFFFF"
                            style = {styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>

                <View style = {styles.oneButtonView}>
                    <TouchableOpacity style = {styles.touchableStyle}
                        disabled={this.state.layerConfigDisabled}
                        onPress={this.layerConfigHandler}
                        >
                        <Text style = {styles.btnText}>
                            Layer Config
                        </Text>
                        <Icon
                            size={Dimensions.get('window').height * 0.04}
                            name="md-arrow-dropright"
                            color="#FFFFFF"
                            style = {styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>

                <View style = {styles.oneButtonView}>
                    <TouchableOpacity style = {styles.touchableStyle}
                        disabled={this.state.startCodingDisabled}
                        onPress={this.startCodingHandler}
                        >
                        <Text style = {styles.btnText}>
                            Start Coding
                        </Text>
                        <Icon
                            size={Dimensions.get('window').height * 0.04}
                            name="md-arrow-dropright"
                            color="#FFFFFF"
                            style = {styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>

                <View style = {styles.oneButtonView}>
                    <TouchableOpacity style = {styles.touchableStyle}
                        onPress={this.exitAppHandler}
                        >
                        <Text style = {styles.btnText}>
                            Close
                        </Text>
                        <Icon
                            size={Dimensions.get('window').height * 0.04}
                            name="md-arrow-dropright"
                            color="#FFFFFF"
                            style = {styles.iconStyle}
                        />
                    </TouchableOpacity>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView:{
        flex:1,
        backgroundColor: "red"
    },
    oneButtonView:{
        flex: 1,
        backgroundColor: "#1463ff",
        borderBottomWidth: 0.5,
        borderBottomColor: "white"
    },
    touchableStyle: {
        flex: 1,
        flexDirection: "row",
    },
    btnText: {
        width: "90%",
        height: "100%",
        textAlign: "center",
        textAlignVertical: "center",
        fontSize: Dimensions.get('window').height * 0.04,
        fontWeight: "bold",
        color: "white",
    },
    iconStyle: {
        width: "10%",
        height: "100%",
        textAlignVertical: "center",
    }
    
})

const mapStateToProps = (state) => {
    return{
        currentProject: state.project.currentProject,
        allProjects: state.project.projects
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllProjects: () => dispatch(fetchProjects()),
        getAllLayers: () => dispatch(fetchLayers()),
        changeProjectMode: (projectMode) => dispatch(setProjectMode(projectMode))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen)
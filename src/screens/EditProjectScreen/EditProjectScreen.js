import React, { Component } from 'react'
import { View,Dimensions, Text, TextInput, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ImageBackground } from 'react-native'
import { connect } from 'react-redux'
import * as RNFS from 'react-native-fs'
import imgBack from '../../assets/GeoVCS_background.jpg'
import { editProject, fetchProjects, deleteProject } from '../../store/actions/index'
import { validateProjectName } from '../../utils/validationHelper'

class CreateProjectScreen extends Component {
    state = {
        projectName: ''
    }

    projectNameHandler = ( value ) => {
        this.setState({
            projectName: value
        })
    }

    editProjectHandler = async () => {
        console.log(this.state.projectName)
        if(validateProjectName(this.state.projectName)) {
            const createdProjectNames = this.props.createdProjects.map(project => project.name)
            if(createdProjectNames.includes(this.state.projectName)) {
                alert('Already exists\nPlease choose another project name')
            }
            else {
                await RNFS.copyFile(RNFS.ExternalDirectoryPath + '/Projects/' + this.props.selectedProject + '.txt', RNFS.ExternalDirectoryPath + '/Projects/' + this.state.projectName + '.txt')
                    .then(async () => {
                        alert('Project name changed to ' + this.state.projectName)
                        this.props.editProject(this.state.projectName)
                        await this.props.deleteProject(this.props.projectName)
                        await this.props.getAllProjects();
                        this.props.pop()
                    })
                    .catch(error => {
                        alert('Error changing project name')
                        return;
                    })
            }
        }
        else {
            alert('Project name can not be empty or contain "." or "@"');
        }
    }

    cancelEditingProject = () => {
        this.props.navigator.pop();
    }

    componentWillUnmount() {
        this.props.enableProjectControls()
    }

    render() {
        return (
            <ImageBackground 
                source={imgBack}
                style = {styles.container}>
                    <View style={styles.inputBox}>
                        <Text style={styles.label}>Enter New Project Name</Text>
                        <TextInput 
                            style={styles.input} 
                            placeholder="New project name ..." 
                            placeholderTextColor="#b8bfb8"
                            value={this.state.projectName}
                            onChangeText={this.projectNameHandler} />
                        <View style={styles.buttonsContainer}>
                            <TouchableOpacity style = {{opacity: 0.9, flex:1, alignItems:"flex-start"}} onPress={() => {this.editProjectHandler(this.props.projectName)}}>
                                    <Text style = {[styles.btnForm, {backgroundColor:"green", borderColor:"#00ff44"}]}>Edit</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style = {{opacity: 0.9, flex:1, alignItems:"flex-end"}} onPress={() => {this.cancelEditingProject(this.props.projectName)}}>
                                    <Text style = {[styles.btnForm, {backgroundColor:"red", borderColor:"orange"}]}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('window').height,
        alignItems: "center",
    },
    inputBox: {
        margin: "auto",
        width: Dimensions.get('window').width * 0.95,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    label: {
        margin: Dimensions.get('window').height * 0.01,
        fontSize: Dimensions.get('window').height * 0.03,
        fontWeight: "bold",
        color: "white"
    },
    input: {
        height: "20%",
        fontSize: Dimensions.get('window').height * 0.02,
        fontWeight: "bold",
        width: "95%",
        borderWidth: 1,
        borderColor: "#8c918c",
        margin: Dimensions.get('window').height * 0.02,
        backgroundColor: "white",
        borderRadius: 10,
        color: "black",
    },
    buttonsContainer: {
        height: "20%",
        flexDirection: "row",
        justifyContent:"space-around",
        margin: Dimensions.get('window').height * 0.02,
    },
    btnForm: {
        borderWidth: 2,
        fontSize: Dimensions.get('window').height * 0.03,
        fontWeight: "bold",
        color: 'white',
        width: "80%",
        height: "100%",
        textAlign: "center",
        textAlignVertical:"center",
        
    },
  
})

const mapStateToProps = (state) => {
    return {
        currentProject: state.project.currentProject,
        createdProjects: state.project.projects,
        selectedProject: state.project.selectedProject
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        editProject: (projectName) => dispatch(editProject(projectName)),
        deleteProject: (projectName) => dispatch(deleteProject(projectName)),
        getAllProjects: () => dispatch(fetchProjects())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProjectScreen)
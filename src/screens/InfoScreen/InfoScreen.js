import React from 'react';
import { View, Text, Dimensions, StyleSheet, ScrollView, Image, Linking } from 'react-native'
import { Title } from 'react-native-paper';
import img1 from '../../assets/img1.jpg'
import img2 from '../../assets/img2.png'
import img3 from '../../assets/img3.png'
import img4 from '../../assets/img4.jpg'
import img5 from '../../assets/img5.jpg'
import img6 from '../../assets/img6.jpg'
import img7 from '../../assets/img7.png'

class InfoScreen extends React.Component {
    componentWillUnmount() {
        //this.props.enableButtons();
    }

    render() {
        return (
        <ScrollView style={styles.scrollContainer}>
    
                <Text style={styles.title}>GeoVCS</Text>
                <Text style={styles.text}>
                    <Text style={styles.boldedText}>GeoVCS</Text> is smart application intended primarily for simple surveying instruments without graphic possibilities. It enables Coding and Linework by storing additional information for each observed point, using manual and voice input in an intuitive way. Logic behind application is sequential and simplified in order to minimize interaction with mobile device while storing enough information to create DXF file with all the corresponding strings and layers. Each line has to be surveyed from beginning to the end without jumping between the strings.
                </Text>
                <View style = {styles.viewImg}>
                    <Image 
                        source={img1}
                        style = {styles.imgStyle}>
                    </Image>
                </View>
                <Text style={styles.text}>
                    Basics of the logic are as following:
                </Text>
                <Text style={styles.text}>
                   1.	E, N and H coordinates are sequentially stored for each observed point in surveying instrument (Total station or GNSS) 
                </Text>
                <View style = {styles.viewImg2}>
                    <Image 
                        source={img2}
                        style = {styles.imgStyle2}>
                    </Image>
                </View>
                <Text style={styles.text}>
                    2.	At the same time, combining manual and voice input, GeoVCS is storing information about Point ID, Code and Linework.
                </Text>
                <Text style={styles.textCenter}>
                    {"\t\t\t201 11 ZGS BL\n\t\t\t202 11 ZGS L\n\t\t\t203 11 ZGS L\n\t\t\t204 11 ZGS L\n\t\t\t205 11 ZGS EL\n\t\t\t206 41 KT P\n\t\t\t207 41 KT P\n\t\t\t208 99 XX P"}

                </Text>
                <Text style={styles.text}>
                    3.	Key link between coordinate file and additional Code and Linework information on mobile phone is <Text style={styles.boldedText}>Point ID</Text> which has to be identical.
                </Text>
                <Text style={styles.text}>
                    4.	Once your survey is finished you combine coordinate file (Instrument data) and coding information (Mobile phone file) in to <Text style={styles.boldedText}>DXF</Text> file using Windows <Text style={styles.boldedText}>GeoVCS Desktop</Text> app. Created DXF file contains all the observed points and strings in appropriate layers.
                </Text>
                <View style = {styles.viewImg2}>
                    <Image 
                        source={img3}
                        style = {styles.imgStyle2}>
                    </Image>
                </View>
                <Text style={styles.text}>
                    <Text style={styles.boldedText}>GeoVCS Desktop</Text>
                    {"app can be found at link: "}
                    <Text style={{color: 'blue'}}
                            onPress={() => Linking.openURL('https://www.geomodel.hr/geovcs/GeoVCS.zip')}>
                                https://www.geomodel.hr/geovcs/GeoVCS.zip
                    </Text>
                    {" and is completely free. Application does not have an installation. All you need to do is to copy geovcs.exe on your desktop.\n"}
                    </Text>
                <Text style={styles.boldedText}>
                    STORING INFORMATION IN MOBILE GeoVCS APP
                </Text>
                <Text style={styles.text}>
                    {"1.	There are two types of objects:\n\ta. LINE\n\tb. POINT"}
                </Text>
                <View style = {styles.viewImg}>
                    <Image 
                        source={img4}
                        style = {styles.imgStyle}>
                    </Image>
                </View>
                <Text style={styles.text}>
                    
                        2.	For <Text style={styles.boldedText}>LINE</Text> option you are required to enter Layer Number (e.g. <Text style={styles.boldedText}>11</Text>), Layer Name (e.g. <Text style={styles.boldedText}>ZGS</Text>) and additional Linework info (<Text style={styles.boldedText}>BL-begin line, L-line, EL – end line</Text>).
                </Text>
                <Text style={styles.text}>
                        3. For <Text style={styles.boldedText}>POINT</Text> option you are required to enter Layer Number (e.g. <Text style={styles.boldedText}>11</Text>), Layer Name (<Text style={styles.boldedText}>ZGS</Text>) and additional info (<Text style={styles.boldedText}>P-point</Text>).
                </Text>
                <Text style={styles.text}>
                        4. To start the line you only need to choose <Text style={styles.boldedText}>BL</Text> (Begin Line) command.
                </Text>

                <View style = {styles.viewImg}>
                    <Image 
                        source={img5}
                        style = {styles.imgStyle}>
                    </Image>
                </View>
                <Text style={styles.text}>
                    5.	Choosing different code or opening new string automatically closes previous line.
                </Text>
                <Text style={styles.text}>
                    6.	As for POINT type, once you pick code for your point it stays active until you change to different code.
                </Text>
                <Text style={styles.text}>
                    7.	If you don't want to add any additional info to certian observed points you should use command <Text style={styles.boldedText}>99 XX</Text>. That command stays active until you use another command and all the points surveyed in between two commands will have no additional information stored.
                </Text>
                <Text style={styles.text}>
                    8.	Saved TXT file can be shared using 'Share' button through multiple file-sharing options (<Text style={styles.boldedText}>email, Google Drive, Whatsapp</Text> etc.)
                </Text>
                <View style = {styles.viewImg}>
                    <Image 
                        source={img6}
                        style = {styles.imgStyle}>
                    </Image>
                </View>
                <Text style={styles.text}>
                Free version is limited to 1 project with max. 50 points. Full PRO version with all the functionalities is available for $12 US.
                </Text>
        </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    title:{
        //padding: 20,
        textAlign: "center",
        fontSize: Dimensions.get('window').height * 0.04,
        color: "black",
        fontWeight: "bold"

    },
    scrollContainer: {
        
        //width: Dimensions.get('window').width,
        backgroundColor: 'white',
        flex: 1,
        margin: 10
    },
    imgStyle:{
        //433*938
        flex: 1,
        resizeMode: "contain",
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        margin: 1
    },
    imgStyle2:{
        //433*938
        //flex: 1,
        resizeMode: "contain",
        width: Dimensions.get('window').width,
        //height: Dimensions.get('window').height,
        margin: 1
    },
    text: {
        margin: 10,
        color: "black"
    },
    viewImg:{
        //width: "100%",
        marginTop: 5,
        alignItems: "center"
    },
    viewImg2:{
        //width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: 'auto'
    },
    boldedText:{
        fontWeight: "bold",
        color:"black"
    },
    textCenter:{
        color: "black",
        fontWeight: "bold",
    }
})

export default InfoScreen
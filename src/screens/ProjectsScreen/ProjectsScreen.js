import React, { Component } from 'react'
import { View, FlatList, StyleSheet, ScrollView} from 'react-native'
import { connect } from 'react-redux'

import Project from '../../components/Project/Project'
import { setProjectMode, selectProject } from '../../store/actions/index'

class ProjectsScreen extends Component {

    componentWillUnmount() {
        this.props.setProjectMode('')
        this.props.selectProject('')
        this.props.enableButtons();
    }

    render() {
        return (
            <ScrollView style= {styles.scrollViewStyle}>
                <View style= {styles.mainView}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        style={styles.list}
                        data={this.props.projects}
                        renderItem={(info) => <Project 
                                                    key={info.index} 
                                                    enableButtons={this.props.enableButtons}
                                                    enableProjectControls={this.enableProjectControls}
                                                    projectName={info.item.name}
                                                    push={this.props.navigator.push}
                                                    pop={this.props.navigator.pop} />} />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    scrollViewStyle:{
        backgroundColor: "#FFFFFF"
    },
    mainView:{
        height: "100%", 
        width: "100%"
    },
    list: {
        width: "100%",
        height: "100%",
        padding: 5,
        flex: 1
    }
})

const mapStateToProps = (state) => {
    return {
        projects: state.project.projects
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setProjectMode: (mode) => dispatch(setProjectMode(mode)),
        selectProject: (project) => dispatch(selectProject(project))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsScreen);
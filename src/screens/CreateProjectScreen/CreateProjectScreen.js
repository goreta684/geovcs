import React, { Component } from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ImageBackground, Dimensions, Alert } from 'react-native'
import * as RNFS from 'react-native-fs'
import { connect } from 'react-redux'
import { createProject } from '../../store/actions/index'
import { validateProjectName } from '../../utils/validationHelper'
import imgBack from '../../assets/GeoVCS_background.jpg'

class CreateProjectScreen extends Component {
    state = {
        projectName: ''
    }

    projectNameHandler = ( value ) => {
        this.setState({
            projectName: value
        })
    }

    createProjectHandler = async () => {
        if(validateProjectName(this.state.projectName)) {
            const createdProjectNames = this.props.createdProjects.map(project => project.name)
            if(createdProjectNames.includes(this.state.projectName)) {
                alert('Already exists\nPlease choose another project name')
            }
            else {
                await RNFS.mkdir(RNFS.ExternalDirectoryPath + '/Projects')
                await RNFS.writeFile(RNFS.ExternalDirectoryPath + '/Projects/' + this.state.projectName + '.txt', this.state.projectName + '\n', 'utf8')
                .then(result => {})
                .catch(error => alert(error))
                await this.props.createNewProject(this.state.projectName);
                await alert("New Project Successfully created!")
                this.props.navigator.pop();
            }
        }
        else {
            alert('Project name can not be empty or contain "." or "@"');
        }
    }

    cancelCreatingProject = () => {
        this.props.navigator.pop();
    }

    componentWillUnmount() {
        this.props.enableButtons();
    }

    render() {
        return (
            <ImageBackground 
                source={imgBack}
                style = {styles.container}
                >
                    <View style={styles.inputBox}>
                        <Text style={styles.label}>Enter Project Name</Text>
                        <TextInput 
                            style={styles.input} 
                            placeholder="Project name ..."
                            placeholderTextColor="#b8bfb8"
                            value={this.state.projectName}
                            onChangeText={this.projectNameHandler} />
                        <View style={styles.buttonsContainer}>
                            <TouchableOpacity style = {{opacity: 0.9, flex:1, alignItems:"flex-start"}} onPress={() => {this.createProjectHandler(this.props.projectName)}}>
                                    <Text style = {[styles.btnForm, {backgroundColor:"green", borderColor:"#00ff44"}]}>Create</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style = {{opacity: 0.9, flex:1, alignItems:"flex-end"}} onPress={() => {this.cancelCreatingProject(this.props.projectName)}}>
                                    <Text style = {[styles.btnForm, {backgroundColor:"red", borderColor:"orange"}]}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                
            </ImageBackground>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('window').height,
        alignItems: "center",
    },
    inputBox: {
        margin: "auto",
        width: Dimensions.get('window').width * 0.95,
        alignItems: "center",
        justifyContent: "flex-start",
        
    },
    label: {
        margin: Dimensions.get('window').height * 0.01,
        fontSize: Dimensions.get('window').height * 0.03,
        fontWeight: "bold",
        color: "white"
    },
    input: {
        height: "20%",
        fontSize: Dimensions.get('window').height * 0.02,
        fontWeight: "bold",
        width: "95%",
        borderWidth: 1,
        borderColor: "#8c918c",
        margin: Dimensions.get('window').height * 0.02,
        backgroundColor: "white",
        borderRadius: 10,
        color: "black",
    },
    buttonsContainer: {
        height: "20%",
        flexDirection: "row",
        justifyContent:"space-around",
        margin: Dimensions.get('window').height * 0.02,
    },
    btnForm: {
        borderWidth: 2,
        fontSize: Dimensions.get('window').height * 0.03,
        fontWeight: "bold",
        color: 'white',
        width: "80%",
        height: "100%",
        textAlign: "center",
        textAlignVertical:"center",
        
    },
  
})

const mapStateToProps = (state) => {
    return {
        currentProject: state.project.currentProject,
        createdProjects: state.project.projects
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createNewProject: (projectName) => dispatch(createProject(projectName)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProjectScreen)
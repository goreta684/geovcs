import React, { Component } from 'react'
import { 
    View, 
    Text, 
    TextInput, 
    StyleSheet, 
    TouchableOpacity,  
    ScrollView, ImageBackground, Dimensions } from 'react-native'
import * as RNFS from 'react-native-fs'
import { connect } from 'react-redux';
import { RadioButton } from 'react-native-paper'
import { createLayer, updateLayers } from '../../store/actions/index'
import { validateLayerName } from '../../utils/validationLayerHelper'
import imgBack from '../../assets/GeoVCS_background.jpg'

const LAYERS_PATH = RNFS.ExternalDirectoryPath + "/Layers/layers.txt";

class LayerConfigScreen extends Component {
    state = {
        layer: {
            layerNumber: '',
            layerName: '',
            layerType: 'point'
        },
        layerMode: 'create',
        layerColor: 'red'
    }
    
    layerNumberHandler = ( value ) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                layer: { 
                    ...prevState.layer,
                    layerNumber: value
                }
            }
        })
    }

    layerNameHandler = ( value ) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                layer: {
                    ...prevState.layer,
                    layerName: value
                }
            }
        })
    }

    layerTypeHandler = ( value ) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                layer: { 
                    ...prevState.layer,
                    layerType: value
                }
            }
        })
    }

    checkLayerNumber = (value) => {
        if (value.includes(" ") || value.includes(",") || value.includes(".") || value.includes("-")){
            return false
        }
        return true
     }

    checkLayerConfig = (value1, value2, value3) => {
         if (value1 ==="" || value2 ==="" || value3===""){
             return false;
         }
         return true;
     }

    checkLayerExist = (value, layersArray) => {
        if (layersArray.includes(value)){
            return false;
        }
        return true;
     }

    createLayerHandler = async () => {
        if (validateLayerName(this.state.layer.layerName) && this.checkLayerNumber(this.state.layer.layerNumber) && this.checkLayerConfig(this.state.layer.layerName, this.state.layer.layerNumber, this.state.layer.layerType)){
            const createdLayerNumbers = this.props.createdLayers.map(layer => layer.layerNumber)

            if (createdLayerNumbers.includes(this.state.layer.layerNumber) || this.state.layer.layerNumber === "99"){
                alert('Already exist.\nPlease choose another layer number.\nLayer with number 99 is a special type of layer and cannot be defined.')
            }
            else{
                const contents = "\n" + this.state.layer.layerNumber + " " + this.state.layer.layerName + " " + this.state.layer.layerType
                await RNFS.mkdir(RNFS.ExternalDirectoryPath + '/Layers')
                await this.props.createNewLayer(this.state.layer);
                await RNFS.appendFile(LAYERS_PATH, contents, 'ascii')
                    .then((result) => {
                        alert("New layer successfully created!")
                        this.resetForm()
                    })
                    .catch((error) => {
                        
                    })
            }
        }else{
            alert("The fields can not be empty and must contain only the English alphabet, numbers 0-9, signs '_' and '-'.")
        }
    }

    toggleEditing = (layer) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                layer: layer,
                layerMode: 'edit'
            }
        })
    }

    editLayerHandler = () => {
        let fileContent = ''

        const layers = this.props.createdLayers
        const editedLayers = layers.map((layer) => {
            if (layer.layerNumber === this.state.layer.layerNumber) {
                return this.state.layer
            }
            return layer
        })

        this.props.updateLayers(editedLayers);
        for(const layer of editedLayers) {
            fileContent += "\n" + layer.layerNumber + " " + layer.layerName + " " + layer.layerType
        }
        this.saveLayersToFile(fileContent, 'edit')
    }

    deleteLayerHandler = () => {
        let fileContent = '';
        if (this.state.layerMode === 'edit') {
            const layers = this.props.createdLayers
            const updatedLayers = layers.filter((layer) => {
                return layer.layerNumber !== this.state.layer.layerNumber
            })

            this.props.updateLayers(updatedLayers)
            for(const layer of updatedLayers) {
                fileContent += "\n" + layer.layerNumber + " " + layer.layerName + " " + layer.layerType
            }
            this.saveLayersToFile(fileContent, 'delete')
        }
        else {
            alert('You must choose layer to delete')
        }
    }

    saveLayersToFile = (fileContent, action) => {
        RNFS.writeFile(LAYERS_PATH, fileContent, 'ascii')
            .then((result) => {
                alert(`Layer successfuly ${action === 'edit' ? 'edited' : 'deleted'}`)
                this.resetForm()
            })
            .catch((error) => {
                
            })
    }

    layerHandler = () => {
        if (this.state.layerMode === 'create') {
            this.createLayerHandler();
        }
        else {
            this.editLayerHandler()
        }
    }

    resetForm = () => {
        this.setState({
            layer: {
                layerNumber: '',
                layerName: '',
                layerType: 'point'
            },
            layerMode: 'create'
        })
    }

    componentWillUnmount = () => {
        this.props.enableButtons()
    }
    
    render() {
        return (
            <ImageBackground 
                source={imgBack}
                style = {styles.mainImg}
                >
            <View style = {styles.mainView}>
                <View style = {styles.inputAndBtnContainer}>
                    <View style = {styles.inputContainer}>
                        <TextInput
                            keyboardType={"numeric"}
                            style={styles.input}
                            placeholder=" Layer Number: "
                            placeholderTextColor="#b8bfb8" 
                            onChangeText={this.layerNumberHandler}
                            value={this.state.layer.layerNumber}
                            editable={this.state.layerMode === 'create' ? true : false} 
                        />
                        <TextInput 
                            style={styles.input} 
                            placeholder=" Layer Name: "
                            placeholderTextColor="#b8bfb8" 
                            onChangeText={this.layerNameHandler}
                            value={this.state.layer.layerName} 
                        />
                        <View style ={styles.rbInput}>
                            <RadioButton.Group
                                onValueChange={layerType => this.layerTypeHandler(layerType)}
                                value={this.state.layer.layerType}>
                                    <View style = {styles.radioBtnContents}>
                                        <Text style = {styles.radioBtnTitle}>Layer type:</Text>
                                        <View style = {styles.radioBtnItems}>
                                            <View style = {styles.radioBtnItemsValuesAndText}>
                                                <RadioButton 
                                                    value="point"
                                                    color={"black"}
                                                    />
                                                <Text style={styles.rbText}>Point</Text>
                                            </View>
                                            <View style = {styles.radioBtnItemsValuesAndText}>
                                                <RadioButton 
                                                    value="line"
                                                    color={"black"} 
                                                    />
                                                <Text style={styles.rbText}>Line</Text>
                                            </View>
                                        </View>
                                    </View>
                                    
                            </RadioButton.Group>
                        </View>
                    </View>
                    <View style = {styles.btnContainer}>
                        <View style = {styles.btnInContainer}>
                            <TouchableOpacity style = {{opacity: 0.9, flex: 1}}>
                                <Text style = {[styles.btnForm, {backgroundColor:"green", borderColor:"#00ff44"}]}  onPress={() => {this.layerHandler()}}>
                                    {this.state.layerMode === 'create' ? 'Create' : 'Edit'}   
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {styles.btnInContainer}>
                            <TouchableOpacity style = {{opacity: 0.9, flex: 1}}>
                                <Text style = {[styles.btnForm, {backgroundColor:"red", borderColor: "orange"}]} onPress={() => {this.deleteLayerHandler()}}>
                                    Delete  
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {styles.btnInContainer}>
                            <TouchableOpacity style = {{opacity: 0.9, flex:1}}>
                                <Text style = {[styles.btnForm, {backgroundColor: "blue", borderColor:"#0091ff"}]} onPress={() => {this.resetForm()}}>
                                    Reset   
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                </View>
                <View style = {styles.mainLayerContainer}>
                    <ScrollView style={styles.layersContainer}>
                            {this.props.createdLayers.map(layer => {
                                return (
                                    <TouchableOpacity 
                                        key={layer.layerNumber}
                                        style={styles.layer} 
                                        onPress={() => this.toggleEditing(layer)}>
                                        <Text style={styles.layerPreview}>
                                            {`${" "} ${layer.layerNumber} ${" "} ${layer.layerName}  ${" "} ${layer.layerType}`}
                                        </Text>
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                </View>
            </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    mainImg:{
       height: Dimensions.get('window').height

    },
    mainView:{
        flex: 1,
        margin: 5
        
    },
    inputAndBtnContainer: {
        height: "38%",
        marginBottom: 2
        
    },
    mainLayerContainer:{
        height: "40%",
        borderColor: "#e3e8e3",
        borderWidth:3,
        marginTop: 5
        
    },
    inputContainer: {
        height: "80%",
        marginBottom: 2
        
    },
    input: {
      backgroundColor: "white",
      marginTop: 2,
      marginBottom: 2,
      fontSize: Dimensions.get('window').height * 0.02,
      color: "black",
      fontWeight: "bold",
      //flex: 1,
      borderColor: "#8c918c",
      borderWidth: 1
    },
    rbInput:{
        backgroundColor: "white",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 2,
        marginBottom: 2,
        borderColor: "#8c918c",
        borderWidth: 1
        
    },
    radioButton:{
        
    },
    radioBtnContents:{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "95%"
    },
    radioBtnTitle:{
        color: "#b8bfb8",
        fontWeight: "bold",
        fontSize: Dimensions.get('window').height * 0.02,
        width: "30%"
    
    },
    radioBtnItems:{
        flexDirection: "row",
        width: "70%",
        justifyContent: "center",
        alignItems: "center"
        
        
    },
    radioBtnItemsValuesAndText: { 
        flexDirection: "row",
        alignItems: "center",
    },
    btnContainer: {
        height: "20%",
        flexDirection: "row",
    },
    btnInContainer: {
        flex: 1,
        
    },
    btnForm: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
        textAlignVertical: "center",
        fontSize: Dimensions.get('window').height * 0.02,
        fontWeight: "bold",
        color: "white",
         
    },
    rbText:{
        fontSize: Dimensions.get('window').height * 0.02,
        fontWeight: "bold",
        color: "black"
    },
    
    layersContainer: {
        
        
    },
    layer: {
        height: 30,
        margin: 2,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: "white",
        
        
    },
    layerPreview: {
        flex: 1,
        //alignItems: "center",
        justifyContent: "center",
        //textAlign: "center",
        textAlignVertical: "center",
        fontSize: Dimensions.get('window').height * 0.02,
        fontWeight: "bold",
        color: "black",
        
        
    },
    textInputContainer: {
        
    }
})

const mapStateToProps = (state) => {
    return{
        currentLayer: state.layer.currentLayer,
        createdLayers: state.layer.layers,
        currentProject: state.project.currentProject
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createNewLayer: (layer) => dispatch(createLayer(layer)),
        updateLayers: (layers) => dispatch(updateLayers(layers))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LayerConfigScreen)
import { Navigation } from 'react-native-navigation'
import Icon from 'react-native-vector-icons/Ionicons'

const startApp = () => {
    Promise.all([
        Icon.getImageSource('md-information-circle', 30)
    ]).then(sources => {
        Navigation.startSingleScreenApp({
            screen: {
                screen: "geovcs.StartScreen",
                title: "GeoVCS",
                navigatorButtons: {
                    rightButtons: [
                        {
                            icon: sources[0],
                            title: "Info",
                            id: "infoButton",
                            size: 30
                        }
                    ]
                }
            },
            appStyle: {
                orientation: 'portrait'
            },
            drawer: {
                right: {
                    screen: "geovcs.Info"
                }
            }
        })
    })
}

export default startApp
import * as actionTypes from './actionTypes'
import * as RNFS from 'react-native-fs'

export const fetchLayers = () => {
    return async(dispatch) => {
        let layers = []
        RNFS.readFile(RNFS.ExternalDirectoryPath+"/Layers/layers.txt","utf8").then((response)=>{
            const result = response.split('\n').slice(1)
            layers = result.map(layer => {
                const splitedLayer = layer.split(' ')
                return {
                    layerNumber: splitedLayer[0],
                    layerName: splitedLayer[1],
                    layerType: splitedLayer[2]
                }
            })
            dispatch({
                type: actionTypes.FETCH_LAYERS,
                layers: layers
            })
        })
        .catch(error => {
            return;
        })
    }
}

export const updateLayers = (layers) => {
    return{
        type: actionTypes.UPDATE_LAYERS,
        updatedLayers: layers
    }
}

export const createLayer = (layer) => {
    return {
        type: actionTypes.CREATE_LAYER,
        createdLayer: layer
    }
}
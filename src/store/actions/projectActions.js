import * as actionTypes from './actionTypes'

import * as RNFS from 'react-native-fs'

export const createProject = (projectName) => {
    return {
        type: actionTypes.CREATE_PROJECT,
        createdProject: projectName
    }
}

export const selectProject = (projectName) => {
    return {
        type: actionTypes.SELECT_PROJECT,
        selectedProject: projectName
    }
}

export const fetchProjects = () => {
    return async(dispatch) => {
        let projects = []
        await RNFS.readDir(RNFS.ExternalDirectoryPath + '/Projects').then(result => {
            projects = result.map(pr => {
                return {
                    name: pr.name.replace('.txt', ''),
                    dateCreated: new Date(pr.mtime)
                }
            })
        })
        .catch(error => {
            return;
        })
        dispatch({
            type: actionTypes.FETCH_PROJECTS,
            projects: projects
        })
    }
}

export const openProject = (projectName) => {
    return {
        type: actionTypes.OPEN_PROJECT,
        openedProject: projectName
    }
}

export const setProjectMode = (projectMode) => {
    return {
        type: actionTypes.SET_PROJECT_MODE,
        mode: projectMode
    }
}

export const editProject = (newProject) => {
    return async (dispatch) => {
        await dispatch({
            type: actionTypes.EDIT_PROJECT,
            editedProject: newProject
        })
    }
}

export const deleteProject = (projectName) => {
    return async (dispatch) => {
        await RNFS.unlink(RNFS.ExternalDirectoryPath + '/Projects/' + projectName + '.txt')
            .then(() => {
                alert('Project ' + projectName + ' successfully deleted!')
            })
            .catch(error => {
                return;
            })
        dispatch({
            type: actionTypes.DELETE_PROJECT,
            projectName: projectName
        })
    }
}
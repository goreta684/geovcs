import * as actionTypes from '../actions/actionTypes'

const initState = {
    currentLayer: "",
    layers: [],
    layersMode: ''
}

export default layerReducer = (state = initState, action) => {
    switch(action.type) {
        case actionTypes.CREATE_LAYER:
            let updatedLayers = [...state.layers];
            updatedLayers.push(action.createdLayer)
            return {
                ...state,
                currentLayer: action.createdLayer,
                layers: updatedLayers
            }
        case actionTypes.FETCH_LAYERS:
            return {
                ...state,
                layers: action.layers
            }
        case actionTypes.UPDATE_LAYERS:
            return {
                ...state,
                layers: action.updatedLayers
            }
        default: 
            return state
    }
}
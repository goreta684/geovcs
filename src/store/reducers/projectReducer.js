import * as actionTypes from '../actions/actionTypes'

const initState = {
    currentProject: "",
    projects: [],
    projectsMode: '',
    selectedProject: ''
}

export default projectReducer = (state = initState, action) => {
    switch(action.type) {
        case actionTypes.CREATE_PROJECT:
            let updatedProjects = [...state.projects];
            updatedProjects.push({
                name: action.createdProject,
                dateCreated: new Date()
            })
            return {
                ...state,
                currentProject: action.createdProject,
                projects: updatedProjects
            }
        case actionTypes.FETCH_PROJECTS:
            return {
                ...state,
                projects: action.projects
            }
        case actionTypes.OPEN_PROJECT:
            return {
                ...state,
                currentProject: action.openedProject
            }
        case actionTypes.SET_PROJECT_MODE: 
            return {
                ...state,
                projectsMode: action.mode
            }
        case actionTypes.DELETE_PROJECT:
            const projects = [...state.projects]
            let newProjects = projects.filter((project) => project.name !== action.projectName)
            return {
                ...state,
                projects: newProjects,
                currentProject: ""
            }
        case actionTypes.EDIT_PROJECT:
            return {
                ...state,
                currentProject: action.editedProject
            }
        case actionTypes.SELECT_PROJECT:
            return {
                ...state,
                selectedProject: action.selectedProject
            }
        default: 
            return state
    }
}
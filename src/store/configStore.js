import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import projectReducer from './reducers/projectReducer'
import layerReducer from './reducers/layerReducer'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default configurateStore = () => {
    return createStore(combineReducers({
        project: projectReducer,
        layer: layerReducer
    }), composeEnhancers(applyMiddleware(thunk)))
}

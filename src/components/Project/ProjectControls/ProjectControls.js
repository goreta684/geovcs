import React from 'react'
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const ProjectControls = (props) => {
    
    let controls = null;
    if(props.show) {
        controls = (
            <View style={styles.container}>
                <TouchableOpacity 
                    disabled={props.editProjectDisabled}
                    onPress={props.editProject} 
                    style = {styles.editOpenDeleteControl}>
                    <Icon   
                        size={Dimensions.get('window').height * 0.04}
                        name="md-create"
                        color="#000000" />
                </TouchableOpacity>
                <TouchableOpacity 
                    disabled={props.viewProjectDisabled}
                    onPress={props.openFile} 
                    style = {styles.editOpenDeleteControl}>
                    <Icon 
                        size={Dimensions.get('window').height * 0.04}
                        name="md-document"
                        color="#4585f5" />
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={props.deleteProject} 
                    style = {styles.editOpenDeleteControl}>
                    <Icon   
                        size={Dimensions.get('window').height * 0.04}
                        name="md-trash"
                        color="#ed0404" />
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={props.shareProject} 
                    style = {styles.shareControl}>
                    <Icon   
                        size={Dimensions.get('window').height * 0.04}
                        name="md-share"
                        color="#17ed04" />
                </TouchableOpacity>
            </View>
        )
    }
    return (
        <View>
            {controls}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: Dimensions.get('window').height * 0.08,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row",
        backgroundColor: "#FFFFFF",
        flex: 1
    },
    editOpenDeleteControl:{
        borderRightWidth: 1, 
        borderRightColor: "black",  
        flex: 1, 
        alignItems: "center"
    },
    shareControl:{
        flex: 1, 
        alignItems: "center"
    }
})

export default ProjectControls
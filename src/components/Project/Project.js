import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Alert, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import Share from 'react-native-share'
import FileProvider from 'react-native-file-provider'
import * as RNFS from 'react-native-fs'
import * as base64 from 'base-64'

import ProjectControls from './ProjectControls/ProjectControls'
import { openProject, deleteProject, selectProject } from '../../store/actions/index'

class Project extends Component {
    state = {
        editProjectDisabled: false,
        viewProjectDisabled: false
    }

    enableProjectControls = () => {
        this.setState({
            editProjectDisabled: false,
            viewProjectDisabled: false
        })
    }

    selectProjectHandler = (projectName) => {
        if(this.props.projectsMode === 'open') {
            Alert.alert(
                projectName,
                'Do you want to open this project?',
                [
                  {text: 'NO', onPress: () => {return;}},
                  {text: 'YES', onPress: () => {
                    this.props.openProject(projectName)
                    this.props.push({
                        screen: "geovcs.CodeScreen",
                        title: projectName,
                        passProps: {
                            push: this.props.push,
                            enableButtons: this.props.enableButtons
                        }
                    })
                  }},
                ]
              )
        }   
        else {
            this.props.selectProject(projectName)
        }
    }

    editProjectHandler = (currentName) => {
        this.setState({
            editProjectDisabled: true
        })
        this.props.push({
            screen: "geovcs.EditProjectScreen",
            title: "Edit project " + currentName,
            passProps: {
                projectName: currentName,
                pop: this.props.pop,
                enableProjectControls: this.enableProjectControls
            }
        })
    }

    openFileHandler = (projectName) => {
        this.setState({
            viewProjectDisabled: true
        })
        this.props.push({
            screen: "geovcs.PointsPreviewScreen",
            title: projectName + ' points preview',
            passProps: {
                pop: this.props.pop,
                projectName: projectName,
                enableProjectControls: this.enableProjectControls
            }
        })
    }

    shareProjectHandler = (projectName) => {
        if(FileProvider) {
            FileProvider.getUriForFile('com.geovcs.fileprovider', `${RNFS.ExternalDirectoryPath}/Projects/${projectName}.txt`)
                .then((contentUri) => {
                    RNFS.readFile(`${RNFS.ExternalDirectoryPath}/Projects/${projectName}.txt`, 'base64')
                        .then(res => {
                            Share.open({
                                message: projectName,
                                url: `data:text/plain;base64,${res}`,
                                showAppsToView: true
                            }).then(response => alert('File successfully shared'))
                                .catch(error => console.log(error))
                        })
                });
        }
    }

    deleteProjectHandler = (projectName) => {
        Alert.alert(
            projectName,
            'Do you want to delete project ' + projectName + '?',
            [
              {text: 'NO', onPress: () => {return;}},
              {text: 'YES', onPress: () => {
                Alert.alert(
                    projectName,
                    'If you delete your project all content will be lost! \nDo you want to delete project ' + projectName + '?',
                    [
                      {text: 'NO', onPress: () => {return;}},
                      {text: 'YES', onPress: () => {
                          
                        this.props.deleteProject(projectName)
                      }}
                    ]
                  )
              }}
            ]
          )
    }

    render() {
        return (
            <View style = {styles.mainView}>
                <TouchableOpacity onPress={() => {this.selectProjectHandler(this.props.projectName)}}>
                    <View style={[styles.container, this.props.projectName === this.props.currentProject ? styles.currentProject : null]}>
                        <Text style={styles.projectName}>{this.props.projectName}</Text>
                    </View>
                </TouchableOpacity>
                <ProjectControls 
                    show={this.props.projectName === this.props.selectedProject}
                    projectName={this.props.projectName}
                    viewProjectDisabled={this.state.viewProjectDisabled}
                    editProjectDisabled={this.state.editProjectDisabled}
                    openFile={() => this.openFileHandler(this.props.projectName)}
                    deleteProject={() => {this.deleteProjectHandler(this.props.projectName)}}
                    editProject={() => {this.editProjectHandler(this.props.projectName)}}
                    shareProject={() => this.shareProjectHandler(this.props.projectName)} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
    },
   container: {
       width: "100%",
       backgroundColor: "#FFFFFF",
       alignItems: "center",
       height: Dimensions.get('window').height * 0.08,
       justifyContent: "center",
       marginTop: Dimensions.get('window').height * 0.01,
       borderTopColor: "#000000",
       borderLeftColor: "#FFFFFF",
       borderRightColor: "#FFFFFF",
       borderBottomColor: "#FFFFFF",
       borderWidth: 1   
   },
   currentProject: {
       backgroundColor: '#cfffc9',
   },
   projectName: {
       width: "100%",
       fontWeight: "bold",
       fontSize: Dimensions.get('window').height * 0.03,
       color: "#000000",
       textAlign: "center",
   }
})

const mapStateToProps = (state) => {
    return {
        projectsMode: state.project.projectsMode,
        selectedProject: state.project.selectedProject,
        currentProject: state.project.currentProject 
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        openProject: (projectName) => dispatch(openProject(projectName)),
        deleteProject: (projectName) => dispatch(deleteProject(projectName)),
        selectProject: (projectName) => dispatch(selectProject(projectName))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Project)